class AddCoverToEvents < ActiveRecord::Migration
  def up
    add_attachment :events, :cover
  end
  def down
    remove_attachment :events, :cover
  end
end
