class CreateLabelsUsersJoinTable < ActiveRecord::Migration
  def change
    create_table :labels_users, :id => false do |t|
      t.integer :user_id
      t.integer :label_id
    end
  end
end
