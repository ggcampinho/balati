class CreateEntrances < ActiveRecord::Migration
  def change
    create_table :entrances do |t|
      t.decimal :price, :precision => 8, :scale => 2
      t.string :description
      t.integer :event_id
      t.integer :order

      t.timestamps
    end
  end
end
