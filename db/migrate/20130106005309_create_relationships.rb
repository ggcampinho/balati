class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.references :follower, :polymorphic => true
      t.references :following, :polymorphic => true
      
      t.timestamps
    end
    add_index :relationships, [ :follower_id, :follower_type ]
    add_index :relationships, [ :following_id, :following_type ]
  end
end
