class CreateEventsLabelsJoinTable < ActiveRecord::Migration
  def change
    create_table :events_labels, :id => false do |t|
      t.integer :event_id
      t.integer :label_id
    end
  end
end
