class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :owner, :polymorphic => true
      t.references :subject, :polymorphic => true
      t.references :recipient, :polymorphic => true

      t.timestamps
    end
    add_index :activities, [ :owner_id, :owner_type ]
    add_index :activities, [ :subject_id, :subject_type ]
    add_index :activities, [ :recipient_id, :recipient_type ]
  end
end
