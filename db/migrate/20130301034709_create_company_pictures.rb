class CreateCompanyPictures < ActiveRecord::Migration
  def change
    create_table :company_pictures do |t|
      t.references :company
      t.attachment :image
      t.timestamps
    end
  end
end
