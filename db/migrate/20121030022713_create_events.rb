class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.integer :company_id
      t.text :description
      t.datetime :start_time

      t.timestamps
    end
  end
end
