class CreateRsvps < ActiveRecord::Migration
  def change
    create_table :rsvps do |t|
      t.references :event
      t.references :user
      t.string :status

      t.timestamps
    end
    add_index :rsvps, :event_id
    add_index :rsvps, :user_id
  end
end
