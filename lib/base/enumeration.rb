class Enumeration
  @@enumeration = {}
  
  def Enumeration.const_missing(key)
    @@enumeration[key.downcase]
  end   

  def Enumeration.each
    @@enumeration.each do |key,value|
      yield(key,value)
    end
  end

  def Enumeration.values
    @@enumeration.values || []
  end

  def Enumeration.keys
    @@enumeration.keys || []
  end

  def Enumeration.[](key)
    @@enumeration[key]
  end
end