module TrackActivity
  
  def self.included(base)
    base.extend(ClassMethods)
  end
  
  def find_or_create_activity(&block)
    activity = Activity.new
    block.call self, activity
    
    stored = Activity.where(activity.attributes.select {|k,v| [:subject_id, :subject_type, :owner_id, :owner_type, :recipient_id, :recipient_type].include? k.to_sym}).first
    stored || activity
  end
  
  module ClassMethods
    def track_activity(reference = nil, params = nil, &block)
      if reference.blank?
        after_commit "track_activity_#{reference}"
        after_destroy "untrack_activity_#{reference}"
        define_method "track_activity_#{reference}" do
          find_or_create_activity(&block).save
        end
        define_method "untrack_activity_#{reference}" do
          find_or_create_activity(&block).destroy
        end
      elsif attribute_method? reference
        before_save "prepare_track_activity_on_attribute_#{reference}"
        after_commit "track_activity_on_attribute_#{reference}"
        define_method "prepare_track_activity_on_attribute_#{reference}" do
          @track_activity = find_or_create_activity(&block)
        end
        define_method "track_activity_on_attribute_#{reference}" do
          if send("#{reference}_changed?") or @track_activity.new_record?
            action = params[:action]
            if ![:save, :destroy].include?(action)
              action = send action
            end
            @track_activity.send "#{action}"
          end
        end
      end
    end
  end
  
end
