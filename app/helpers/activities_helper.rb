module ActivitiesHelper
  def render_activity(activity)
    partial_name = ("activities/activity_#{activity.owner_type}_#{activity.subject_type}" + (activity.recipient ? "_#{activity.recipient_type}" : "")).downcase
    render :partial => partial_name, :locals => { :activity => activity }
  end
end
