module CompaniesHelper
  def render_pictures(pictures)
    carousel_id = "carousel-#{Time.now.to_i}"
    
    indicators = ""
    pictures.each_index do |i|
      indicators += content_tag :li, nil, :class => (i == 0 ? "active" : nil), :data => { :slide_to => i, :target => "##{carousel_id}" }
    end
    indicators = content_tag :ol, raw(indicators), :class => "carousel-indicators carousel-indicators-#{pictures.size}"
    
    inner = render(pictures)
    inner.gsub! "company-picture", "company-picture item"
    inner.sub! "item", "item active"
    inner = content_tag :div, raw(inner), :class => "carousel-inner"
    
    content_tag :div, indicators + inner, :class => "carousel slide company-pictures-carousel", :id => carousel_id, :data => { :interval => 5000, :auto => true }
  end
end
