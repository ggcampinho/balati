module ApplicationHelper
  def header(title, options = {})
    content_for :header, render("/layouts/menu", :title => title, :back => options[:back])
  end
end
