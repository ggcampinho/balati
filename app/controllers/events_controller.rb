class EventsController < ApplicationController
  before_filter :authenticated
  
  # GET /events or /companies/1/events
  # GET /events.json or /companies/1/events.json
  def index
    @company = Company.includes(:events).find(params[:company_id]) if params.include? :company_id
    @events = @company.events unless @company.blank?
    if @events.blank?
      @events ||= Event.joins('LEFT JOIN events_labels el ON el.event_id = events.id').includes(:labels, :company).group('events.id')
      @events.order("SUM(CASE WHEN el.label_id IN (#{ current_user.label_ids.join "," }) THEN 1 ELSE 0 END) DESC") unless current_user.blank?
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @events }
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.includes(:rsvps).where(:id => params[:id]).first
    
    @rsvp = @event.rsvps.where(:user_id => current_user.id).first if current_user
    @rsvp ||= Rsvp.new :event => @event
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @event }
    end
  end

  # GET /companies/1/events/new
  # GET /companies/1/events/new.json
  def new
    @event = Event.new
    @event.company = Company.find(params[:company_id])
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @event }
    end
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render json: @event, status: :created, location: @event }
      else
        format.html { render action: "new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /events/1
  # PUT /events/1.json
  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @event.update_attributes(params[:event])
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to company_events_url(@event.company) }
      format.json { head :no_content }
    end
  end
  
end
