class AuthenticationsController < ApplicationController
  before_filter :not_authenticated
  
  def create
    omniauth = request.env['omniauth.auth']
    authentication = Authentication.find_by_provider_and_uid(omniauth[:provider], omniauth[:uid])

    if authentication
      authentication.user.picture = StringIO.open URI.parse(omniauth[:info][:image]).read
      authentication.user.save!
      # User is already registered with application
      flash[:info] = 'Signed in successfully.'
      sign_in_and_redirect(authentication.user)
    elsif current_user
      # User is signed in but has not already authenticated with this social network
      current_user.authentications.create!(:provider => omniauth[:provider], :uid => omniauth[:uid])
      current_user.apply_omniauth(omniauth)
      current_user.save

      flash[:info] = 'Authentication successful.'
      redirect_to home_url
    else
      user = User.find_by_email(omniauth[:info][:email]) || User.new
      
      if user.new_record?
        user.apply_omniauth(omniauth)
        user.require_password = false
        user.save
      end
      
      user.authentications.build(:provider => omniauth[:provider], :uid => omniauth[:uid])
      if user.save
        flash[:info] = 'User created and signed in successfully.'
        sign_in_and_redirect(user)
      else
        logger.debug user.authentications.first.errors.to_a.join "\n"
        session[:omniauth] = omniauth.except(:extra)
        redirect_to login_path
      end
    end
  end

  def destroy
    @authentication = current_user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = 'Successfully destroyed authentication.'
    redirect_to authentications_url
  end

  private
  
  def sign_in_and_redirect(user)
    unless current_user
      user_session = UserSession.new user
      user_session.save
    end
    redirect_to root_url
  end
end
