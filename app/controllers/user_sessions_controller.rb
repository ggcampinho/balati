class UserSessionsController < ApplicationController
  before_filter :not_authenticated, :only => [:new, :create]
  before_filter :authenticated, :only => :destroy
  layout 'application'
  
  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      redirect_to back_or_default_url, :notice => "Login successful!"
    else
      render :action => :new
    end
  end

  def destroy
    current_user_session.destroy
    redirect_to back_or_default_url, :notice => "Logout successful!"
  end
end
