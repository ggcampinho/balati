class RsvpsController < ApplicationController
  
  before_filter :authenticated
  
  def create
    event = Event.find(params[:event_id])
    @rsvp = Rsvp.new :status => :attending, :event => event, :user => current_user

    respond_to do |format|
      if @rsvp.save
        @concurrents = @rsvp.decline_concurrents
        format.js
        format.html { redirect_to event, notice: 'Rsvp was successfully created.' }
        format.json { render json: @rsvp, status: :created, location: event }
      else
        format.html { redirect_to event, notice: 'Rsvp cannot be created.' }
        format.json { render json: @rsvp.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @rsvp = Rsvp.includes(:event).where(:id => params[:id]).first
    
    respond_to do |format|
      if @rsvp.user == current_user and @rsvp.update_attributes(params[:rsvp])
        @concurrents = @rsvp.decline_concurrents
        format.js { render :create }
        format.html { redirect_to @rsvp.event, notice: 'Rsvp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { redirect_to @rsvp.event, notice: 'Rsvp cannot be updated.' }
        format.json { render json: @rsvp.errors, status: :unprocessable_entity }
      end
    end
  end
  
end
