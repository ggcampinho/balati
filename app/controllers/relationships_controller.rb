class RelationshipsController < ApplicationController
  before_filter :authenticated
  
  def create
    @relationship = current_user.relationships.build(:following_id => params[:following_id], :following_type => params[:following_type])
    respond_to do |format|
      if @relationship.save
        format.js
        format.html { redirect_to back_or_default_url, :notice => "Successfully created relationship." }
        format.json { render json: @relationship, status: :created }
      else
        format.html { redirect_to back_or_default_url, :error => "Unable to create relationship." }
        format.json { render json: @relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @relationship = Relationship.find(params[:id])
    @relationship.destroy
    
    respond_to do |format|
      format.js { render :create }
      format.html { redirect_to root_url, :notice => "Successfully destroyed relationship." }
      format.json { head :no_content }
    end
  end
end
