class UsersController < ApplicationController
  before_filter :authenticated
  def show
    @user = User.find params[:id]
    render :layout => 'user'
  end
  
  def following
    @user = User.find params[:id]
  end
  
  def followers
    @user = User.find params[:id]
  end
end
