class ActivitiesController < ApplicationController
  before_filter :authenticated
  
  def index
    @user = User.find params[:owner_id] if params[:owner_type] == User.to_s.pluralize.downcase and params[:owner_id]
    @activities
    if !params[:owner_type].blank? and !params[:owner_id].blank?
      @activities = Activity.where(:owner_type => params[:owner_type].singularize.camelize, :owner_id => params[:owner_id]).order('updated_at DESC')
    elsif !current_user.blank?
      @activities = Activity.where("CONCAT(owner_type, '.', owner_id) IN (:following)", { :following => current_user.following.map { |f| "#{f.class.name}.#{f.id}" } }).order('updated_at DESC')
    else
      @activities = Activity.all
    end
  end
end
