$newButton = $(<%= render :partial => 'rsvps/button', :locals => { :rsvp => @rsvp, :event => @rsvp.event } %>)
$oldButton = $(".#{/rsvp-button-event-\d+/.exec $newButton.attr('class')}")

rsvps_ids = [<%== @concurrents.each.map{ |c| c.event_id }.join ',' %>]
$rsvps = $(".rsvp-button")
$.each rsvps_ids, (index, element) ->
	$this = $(".rsvp-button-event-#{element}")
	$this.attr 'href', $this.attr('href').replace('declined', 'attending')
	$this.text 'Eu vou!'

$oldButton.replaceWith $newButton
