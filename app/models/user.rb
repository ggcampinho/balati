class User < ActiveRecord::Base
  
  has_many :relationships, :as => :follower, :dependent => :delete_all
  has_many :inverse_relationships, :class_name => "Relationship", :as => :following, :dependent => :delete_all
  has_many :activities, :as => :owner, :dependent => :delete_all
  has_many :authentications, :dependent => :delete_all
  has_many :rsvps, :dependent => :delete_all
  has_many :events, :through => :rsvps
  has_and_belongs_to_many :labels
  
  has_attached_file :picture,
    :styles => { :original => ['100%', :png], :square => ['180x180#', :png], :medium_square => ['100x100#', :png], :small_square => ['50x50#', :png] },
    :storage => :s3,
    :whiny => false,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "/user/picture/:style/:id.:extension",
    :default_url => ActionController::Base.helpers.asset_path('user/profile.png')

  attr_accessible :name, :gender, :birthday, :email, :password, :password_confirmation, :label_ids
  
  acts_as_authentic do |config|
    config.require_password_confirmation = false
    config.ignore_blank_passwords = true
  end
  
  attr_accessor :require_password
  
  def is_following?(user)
    !relationships.where(:following_id => user.id).first.blank?
  end
  
  def relationship(user)
    relationships.where(:following_id => user.id).first
  end
  
  def following
    relationships.map do |r|
      r.following
    end
  end
  
  def followers
    inverse_relationships.map do |r|
      r.follower
    end
  end
  
  def require_password?
    require_password
  end
  
  def apply_omniauth(omniauth)
    self.picture = StringIO.open URI.parse(omniauth[:info][:image]).read
    self.email = omniauth[:info][:email]
    self.name = omniauth[:info][:name]
    self.gender = omniauth[:extra][:raw_info][:gender]
    date = omniauth[:extra][:raw_info][:birthday].split '/'
    self.birthday = Time.utc date[2], date[0], date[1]
  end
end
