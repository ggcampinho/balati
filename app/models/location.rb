class Location < ActiveRecord::Base
  attr_accessible :city, :complement, :country, :district, :latitude, :longitude, :number, :state, :street, :zip
end
