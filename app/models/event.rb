class Event < ActiveRecord::Base
  
  belongs_to :company
  has_many :entrances, :dependent => :delete_all
  has_many :rsvps, :dependent => :delete_all
  has_many :users, :through => :rsvps
  has_and_belongs_to_many :labels
  
  scope :attending, -> { includes{ rsvps }.where{ rsvps.status == 'attending' } }
  
  has_attached_file :cover,
    :styles => { :original => ['100%', :png], :small => ['480x100#', :png], :medium => ['767x200#', :png], :normal => ['980x300#', :png] },
    :storage => :s3,
    :whiny => false,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "/event/cover/:style/:id.:extension"
  
  track_activity do |event, activity|
    activity.subject = event
    activity.owner = event.company
  end
  
  accepts_nested_attributes_for :entrances, :allow_destroy => true
  
  attr_accessible :company_id, :description, :name, :start_time, :entrances_attributes, :label_ids, :cover
  
  def rsvp(user)
    user ? rsvps.where(:user_id => user.id).first : nil
  end
  
end
