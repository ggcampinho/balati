class Rsvp < ActiveRecord::Base
  extend Enumerize
  
  belongs_to :event
  belongs_to :user
  attr_accessible :status, :event, :user
  
  scope :attending, lambda { |exclude_user = nil|
    s = where{ status == 'attending' }
    s = s.joins{ user.relationships }.where{ (user.relationships.follower_id == exclude_user.id) & (user.relationships.follower_type == exclude_user.class.to_s) } if exclude_user
    s
  }
  
  track_activity :status, :action => :save_or_destroy_activity do |rsvp, activity|
    activity.subject = rsvp
    activity.owner = rsvp.user
    activity.recipient = rsvp.event
  end
  
  enumerize :status, :in => [ :attending, :declined, :automatic_declined ], :predicate => true
  
  def save_or_destroy_activity
    status.attending? ? :save : :destroy
  end
  
  def concurrents
    if new_record?
      Rsvp.concurrents :event => self.event, :user => self.user
    else
      Rsvp.includes(:event).where(['rsvps.id <> ? AND rsvps.user_id = ? AND rsvps.status = ? AND events.start_time >= ? AND events.start_time <= ?', id, user.id, :attending, event.start_time - 6.hours, event.start_time + 6.hours])
    end
  end
  
  def self.concurrents(params = {})
    Rsvp.includes(:event).where(['rsvps.user_id = ? AND rsvps.status = ? AND events.start_time >= ? AND events.start_time <= ?', params[:user].id, :attending, params[:event].start_time - 6.hours, params[:event].start_time + 6.hours])
  end
  
  def decline_concurrents
    c = concurrents
    c.each do |rsvp|
      rsvp.status = :automatic_declined
      rsvp.save!
    end
    c
  end
  
end
