class Entrance < ActiveRecord::Base
  scope :ordered, -> { order :order }
  attr_accessible :description, :event_id, :order, :price
end
