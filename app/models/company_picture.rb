class CompanyPicture < ActiveRecord::Base
  attr_accessible :image
  has_attached_file :image,
    :styles => { :original => ['100%', :png], :small => ['480x200#', :png], :medium => ['767x250#', :png], :normal => ['980x300#', :png] },
    :storage => :s3,
    :whiny => false,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "/company/picture/:style/:id.:extension"
  
end
