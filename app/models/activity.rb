class Activity < ActiveRecord::Base
  belongs_to :owner, :polymorphic => true
  belongs_to :subject, :polymorphic => true
  belongs_to :recipient, :polymorphic => true
end
