class Company < ActiveRecord::Base
  
  belongs_to :location
  has_many :events, :dependent => :delete_all
  has_many :activities, :as => :owner, :dependent => :delete_all
  has_many :relationships, :as => :follower, :dependent => :delete_all
  has_many :inverse_relationships, :class_name => "Relationship", :as => :following, :dependent => :delete_all
  has_many :pictures, :class_name => "CompanyPicture", :dependent => :delete_all
  
  attr_accessible :name, :phone, :location_attributes, :pictures_attributes, :site
  
  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :pictures, :allow_destroy => true
  
  def friends(user)
    company = self
    User.includes{ inverse_relationships }.includes{ rsvps.event.company }
      .where{ inverse_relationships.follower_id == user.id }
      .where{ inverse_relationships.follower_type == "User" }
      .where{ rsvps.event.company.id == company.id }
  end
  
end
