class Relationship < ActiveRecord::Base
  attr_accessible :follower_id, :follower_type, :following_id, :following_type
  belongs_to :follower, :polymorphic => true
  belongs_to :following, :polymorphic => true
end
